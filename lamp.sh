#! /bin/bash

sudo apt-get install apache2
sudo service apache2 enable
sudo service apache2 start 
echo "apache complete!"
sudo service apache2 restart
echo "check status"
sudo service apache2 status
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install php7.0
sudo apt-get install php5.6 
sudo apt-get install php5.6-mysql 
sudo apt-get install php-gettext 
sudo apt-get install php5.6-mbstring 
sudo apt-get install php-mbstring 
sudo apt-get install php7.0-mbstring 
sudo apt-get install php-xdebug 
sudo apt-get install libapache2-mod-php5.6 
sudo apt-get install libapache2-mod-php7.0
sudo a2dismod php7.0
sudo a2enmod php5.6
sudo sevice apache2 restart
sudo apt-get install mysql-server mysql-client
sudo service mysql status
sudo apt-get install phpmyadmin
sudo ln -s /etc/phpmyadmin/apache.conf 
/etc/apache2/conf-available/phpmyadmin.conf
sudo a2enconf phpmyadmin.conf
sudo systemctl reload apache2.service

sudo chmod -R 777 /var/www

